<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'sevices', 'as' => 'api.services.'], function(){
    Route::get('komoditi', 'Api\ServicesController@getKomoditi');
    Route::get('komoditi/detail', 'Api\ServicesController@getKomoditiDetail');
    Route::get('detail-komoditi', 'Api\ServicesController@getDetailKomoditi')->name('detail-komoditi');
    Route::get('search-komoditi', 'Api\ServicesController@searchKomoditi')->name('search.komoditi');
    Route::get('detail-toko/{toko}', 'Api\ServicesController@getDetailToko')->name('detail.toko');
    Route::get('detail-toko-rating', 'Api\ServicesController@getTokoRating')->name('detail.toko.rating');
    Route::post('detail-toko-rating/store', 'Api\ServicesController@storeRating');
});
