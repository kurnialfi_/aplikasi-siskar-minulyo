<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/')
    ->middleware('auth')
    ->group(function () {
        Route::resource('roles', 'RoleController');
        Route::resource('permissions', 'PermissionController');

        Route::get('pemiliks/{pemilik}/generate-account', 'PemilikController@generate')->name('pemiliks.generate');

        Route::resource('bloks', BlokController::class);
        Route::resource('komoditis', KomoditiController::class);
        Route::resource('detail-komoditis', DetailKomoditiController::class);
        Route::resource('tokos', TokoController::class);
        Route::resource('detail-tokos', DetailTokoController::class);
        Route::resource('pemiliks', PemilikController::class);
        Route::resource('pengunjungs', PengunjungController::class);


        // Pemilik
        Route::get('toko', 'Pemilik\TokoController@index')->name('pemilik.toko.index');
        Route::get('rule', 'RuleController@index')->name('rule.index');
        Route::get('rule/toko', 'RuleController@toko')->name('rule.toko');

    });
