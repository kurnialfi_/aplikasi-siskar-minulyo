<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Komoditi;
use Illuminate\Auth\Access\HandlesAuthorization;

class KomoditiPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the komoditi can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list komoditis');
    }

    /**
     * Determine whether the komoditi can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function view(User $user, Komoditi $model)
    {
        return $user->hasPermissionTo('view komoditis');
    }

    /**
     * Determine whether the komoditi can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create komoditis');
    }

    /**
     * Determine whether the komoditi can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function update(User $user, Komoditi $model)
    {
        return $user->hasPermissionTo('update komoditis');
    }

    /**
     * Determine whether the komoditi can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function delete(User $user, Komoditi $model)
    {
        return $user->hasPermissionTo('delete komoditis');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete komoditis');
    }

    /**
     * Determine whether the komoditi can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function restore(User $user, Komoditi $model)
    {
        return false;
    }

    /**
     * Determine whether the komoditi can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Komoditi  $model
     * @return mixed
     */
    public function forceDelete(User $user, Komoditi $model)
    {
        return false;
    }
}
