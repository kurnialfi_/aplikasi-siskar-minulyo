<?php

namespace App\Policies;

use App\Models\User;
use App\Models\DetailKomoditi;
use Illuminate\Auth\Access\HandlesAuthorization;

class DetailKomoditiPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the detailKomoditi can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list detailkomoditis');
    }

    /**
     * Determine whether the detailKomoditi can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function view(User $user, DetailKomoditi $model)
    {
        return $user->hasPermissionTo('view detailkomoditis');
    }

    /**
     * Determine whether the detailKomoditi can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create detailkomoditis');
    }

    /**
     * Determine whether the detailKomoditi can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function update(User $user, DetailKomoditi $model)
    {
        return $user->hasPermissionTo('update detailkomoditis');
    }

    /**
     * Determine whether the detailKomoditi can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function delete(User $user, DetailKomoditi $model)
    {
        return $user->hasPermissionTo('delete detailkomoditis');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete detailkomoditis');
    }

    /**
     * Determine whether the detailKomoditi can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function restore(User $user, DetailKomoditi $model)
    {
        return false;
    }

    /**
     * Determine whether the detailKomoditi can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailKomoditi  $model
     * @return mixed
     */
    public function forceDelete(User $user, DetailKomoditi $model)
    {
        return false;
    }
}
