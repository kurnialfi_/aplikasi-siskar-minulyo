<?php

namespace App\Policies;

use App\Models\Blok;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlokPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the blok can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list bloks');
    }

    /**
     * Determine whether the blok can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function view(User $user, Blok $model)
    {
        return $user->hasPermissionTo('view bloks');
    }

    /**
     * Determine whether the blok can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create bloks');
    }

    /**
     * Determine whether the blok can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function update(User $user, Blok $model)
    {
        return $user->hasPermissionTo('update bloks');
    }

    /**
     * Determine whether the blok can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function delete(User $user, Blok $model)
    {
        return $user->hasPermissionTo('delete bloks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete bloks');
    }

    /**
     * Determine whether the blok can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function restore(User $user, Blok $model)
    {
        return false;
    }

    /**
     * Determine whether the blok can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Blok  $model
     * @return mixed
     */
    public function forceDelete(User $user, Blok $model)
    {
        return false;
    }
}
