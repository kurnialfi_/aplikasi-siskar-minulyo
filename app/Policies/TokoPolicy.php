<?php

namespace App\Policies;

use App\Models\Toko;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TokoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the toko can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list tokos');
    }

    /**
     * Determine whether the toko can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function view(User $user, Toko $model)
    {
        return $user->hasPermissionTo('view tokos');
    }

    /**
     * Determine whether the toko can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create tokos');
    }

    /**
     * Determine whether the toko can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function update(User $user, Toko $model)
    {
        return $user->hasPermissionTo('update tokos');
    }

    /**
     * Determine whether the toko can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function delete(User $user, Toko $model)
    {
        return $user->hasPermissionTo('delete tokos');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete tokos');
    }

    /**
     * Determine whether the toko can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function restore(User $user, Toko $model)
    {
        return false;
    }

    /**
     * Determine whether the toko can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Toko  $model
     * @return mixed
     */
    public function forceDelete(User $user, Toko $model)
    {
        return false;
    }
}
