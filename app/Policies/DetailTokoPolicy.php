<?php

namespace App\Policies;

use App\Models\User;
use App\Models\DetailToko;
use Illuminate\Auth\Access\HandlesAuthorization;

class DetailTokoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the detailToko can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list detailtokos');
    }

    /**
     * Determine whether the detailToko can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function view(User $user, DetailToko $model)
    {
        return $user->hasPermissionTo('view detailtokos');
    }

    /**
     * Determine whether the detailToko can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create detailtokos');
    }

    /**
     * Determine whether the detailToko can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function update(User $user, DetailToko $model)
    {
        return $user->hasPermissionTo('update detailtokos');
    }

    /**
     * Determine whether the detailToko can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function delete(User $user, DetailToko $model)
    {
        return $user->hasPermissionTo('delete detailtokos');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete detailtokos');
    }

    /**
     * Determine whether the detailToko can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function restore(User $user, DetailToko $model)
    {
        return false;
    }

    /**
     * Determine whether the detailToko can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\DetailToko  $model
     * @return mixed
     */
    public function forceDelete(User $user, DetailToko $model)
    {
        return false;
    }
}
