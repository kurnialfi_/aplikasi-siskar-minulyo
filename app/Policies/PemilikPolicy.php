<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Pemilik;
use Illuminate\Auth\Access\HandlesAuthorization;

class PemilikPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the pemilik can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list pemiliks');
    }

    /**
     * Determine whether the pemilik can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function view(User $user, Pemilik $model)
    {
        return $user->hasPermissionTo('view pemiliks');
    }

    /**
     * Determine whether the pemilik can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create pemiliks');
    }

    /**
     * Determine whether the pemilik can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function update(User $user, Pemilik $model)
    {
        return $user->hasPermissionTo('update pemiliks');
    }

    /**
     * Determine whether the pemilik can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function delete(User $user, Pemilik $model)
    {
        return $user->hasPermissionTo('delete pemiliks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete pemiliks');
    }

    /**
     * Determine whether the pemilik can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function restore(User $user, Pemilik $model)
    {
        return false;
    }

    /**
     * Determine whether the pemilik can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pemilik  $model
     * @return mixed
     */
    public function forceDelete(User $user, Pemilik $model)
    {
        return false;
    }
}
