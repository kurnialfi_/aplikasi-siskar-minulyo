<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Pengunjung;
use Illuminate\Auth\Access\HandlesAuthorization;

class PengunjungPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the pengunjung can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list pengunjungs');
    }

    /**
     * Determine whether the pengunjung can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function view(User $user, Pengunjung $model)
    {
        return $user->hasPermissionTo('view pengunjungs');
    }

    /**
     * Determine whether the pengunjung can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create pengunjungs');
    }

    /**
     * Determine whether the pengunjung can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function update(User $user, Pengunjung $model)
    {
        return $user->hasPermissionTo('update pengunjungs');
    }

    /**
     * Determine whether the pengunjung can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function delete(User $user, Pengunjung $model)
    {
        return $user->hasPermissionTo('delete pengunjungs');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete pengunjungs');
    }

    /**
     * Determine whether the pengunjung can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function restore(User $user, Pengunjung $model)
    {
        return false;
    }

    /**
     * Determine whether the pengunjung can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Pengunjung  $model
     * @return mixed
     */
    public function forceDelete(User $user, Pengunjung $model)
    {
        return false;
    }
}
