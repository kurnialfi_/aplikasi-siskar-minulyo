<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DetailTokoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'toko_id' => ['required', 'string', 'exists:toko,id'],
            // 'detail_komoditi_id' => ['required', 'exists:detail_komoditi,id'],
            'pemilik_id' => ['required', 'exists:pemilik,id'],
            'profill_toko' => ['image'],
            'deskripsi' => ['required', 'string'],
        ];
    }
}
