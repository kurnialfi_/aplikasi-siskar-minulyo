<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PemilikStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nik' => ['required', 'unique:pemilik', 'string:16', 'numeric'],
            'nama' => ['required', 'string'],
            'alamat' => ['required', 'string'],
            'no_telp' => ['required', 'unique:pemilik', 'string', 'numeric'],
            'email' => ['required', 'email']
        ];
    }
}
