<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PengunjungUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'detail_toko_id' => ['required', 'exists:detail_toko,id'],
            'nama' => ['required', 'max:255', 'string'],
            'komentar' => ['required', 'max:255', 'string'],
            'rating' => ['required', 'in:1,2,3,4,5'],
        ];
    }
}
