<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TokoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'string'],
            'blok_id' => ['required', 'string', 'exists:blok,id'],
            'no_toko' => ['required', 'string', 'numeric'],
            'komoditi_id' => ['required', 'exists:komoditi,id'],
            'longitude' => ['numeric'],
            'latitude' => ['numeric'],
        ];
    }
}
