<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Toko;
use App\Models\Pemilik;
use App\Models\DetailToko;
use Illuminate\Http\Request;
use App\Models\DetailKomoditi;
use App\Models\DetailTokoKomoditi;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\DetailTokoStoreRequest;
use App\Http\Requests\DetailTokoUpdateRequest;

class DetailTokoController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', DetailToko::class);

        $search = $request->get('search', '');

        // $detailTokos = DetailToko::search($search)
        //     ->latest()
        //     ->paginate(5);

        $detailTokos = DetailToko::select([
            '*',
            DB::raw(
                "( select group_concat(komoditi) from detail_toko_komoditi where detail_toko_id = detail_toko.id ) as komoditi "
            )
        ])
        ->with(['toko'])
        ->latest()
        ->paginate(5);

        return view('app.detail_tokos.index', compact('detailTokos', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', DetailToko::class);

        // $tokos = Toko::pluck('id', 'id', 'komiditi_id');
        $tokos = Toko::all();
        
        $detailKomoditis = DetailKomoditi::all();
//        $pemiliks = Pemilik::pluck('nik', 'id');
        $pemiliks = Pemilik::select("*", DB::raw("CONCAT(pemilik.nik,' (',pemilik.nama,')') as display_name"))
        ->pluck('display_name', 'id');

        return view(
            'app.detail_tokos.create',
            compact('tokos', 'detailKomoditis', 'pemiliks')
        );
    }

    /**
     * @param \App\Http\Requests\DetailTokoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DetailTokoStoreRequest $request)
    {
        $this->authorize('create', DetailToko::class);

        $validated = $request->validated();
        if ($request->hasFile('profill_toko')) {
            $validated['profill_toko'] = $request
                ->file('profill_toko')
                ->store('public');
        }

        DB::beginTransaction();

        $detailToko = DetailToko::create($validated);

        foreach ($request->komoditi as $key => $value) {
            $detailKomoditi = DetailKomoditi::where('id', $value)->first();
            $detail_toko_komoditi = [
                'toko_id' => $request->toko_id,
                'detail_toko_id' => $detailToko->id,
                'detail_komoditi_id' => $detailKomoditi->id,
                'komoditi' => $detailKomoditi->nama
            ];

            DetailTokoKomoditi::create($detail_toko_komoditi);
        }

        DB::commit();

        // DB::rollback();

        return redirect()
            ->route('detail-tokos.edit', $detailToko)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailToko $detailToko
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, DetailToko $detailToko)
    {
        $this->authorize('view', $detailToko);

        // $detailToko = DB::select("
        //     select 
        //         *,
        //         ( select group_concat(komoditi) from detail_toko_komoditi where detail_toko_id = ? ) as komoditi 
        //     from detail_toko where id = ?
        // ", [$detailToko->id, $detailToko->id]);

        $detailToko = DetailToko::select([
            '*',
            DB::raw(
                "( select group_concat(komoditi) from detail_toko_komoditi where detail_toko_id = detail_toko.id ) as komoditi "
            )
        ])
        ->where('id', $detailToko->id)
        ->with(['toko'])
        ->first();

        // $detailToko = collect($detailToko);

        return view('app.detail_tokos.show', compact('detailToko'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailToko $detailToko
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, DetailToko $detailToko)
    {
        $this->authorize('update', $detailToko);

        // $tokos = Toko::pluck('id', 'id');
        $tokos = Toko::all();
        $detailKomoditis = DetailKomoditi::pluck('nama', 'id');
        $pemiliks = Pemilik::select("*", DB::raw("CONCAT(pemilik.nik,' (',pemilik.nama,')') as display_name"))
        ->pluck('display_name', 'id');

        return view(
            'app.detail_tokos.edit',
            compact('detailToko', 'tokos', 'detailKomoditis', 'pemiliks')
        );
    }

    /**
     * @param \App\Http\Requests\DetailTokoUpdateRequest $request
     * @param \App\Models\DetailToko $detailToko
     * @return \Illuminate\Http\Response
     */
    public function update(
        DetailTokoUpdateRequest $request,
        DetailToko $detailToko
    ) {
        $this->authorize('update', $detailToko);

        $validated = $request->validated();

        if ($request->hasFile('profill_toko')) {
            if ($detailToko->profill_toko) {
                Storage::delete($detailToko->profill_toko);
            }

            $validated['profill_toko'] = $request
                ->file('profill_toko')
                ->store('public');
        }

        DB::beginTransaction();

        $detailToko->update($validated);

        DetailTokoKomoditi::where('detail_toko_id', $detailToko->id)->delete();

        foreach ($request->komoditi as $key => $value) {
            $detailKomoditi = DetailKomoditi::where('id', $value)->first();
            $detail_toko_komoditi = [
                'toko_id' => $request->toko_id,
                'detail_toko_id' => $detailToko->id,
                'detail_komoditi_id' => $detailKomoditi->id,
                'komoditi' => $detailKomoditi->nama
            ];

            DetailTokoKomoditi::create($detail_toko_komoditi);
        }

        DB::commit();

        return redirect()
            ->route('detail-tokos.edit', $detailToko)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailToko $detailToko
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DetailToko $detailToko)
    {
        $this->authorize('delete', $detailToko);

        if ($detailToko->profill_toko) {
            Storage::delete($detailToko->profill_toko);
        }

        $detailToko->delete();

        return redirect()
            ->route('detail-tokos.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
