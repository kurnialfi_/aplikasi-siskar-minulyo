<?php

namespace App\Http\Controllers;

use Hash;
use App\Models\User;
use App\Models\Pemilik;
use Illuminate\Http\Request;
use App\Http\Requests\PemilikStoreRequest;
use App\Http\Requests\PemilikUpdateRequest;

class PemilikController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Pemilik::class);

        $search = $request->get('search', '');

        $pemiliks = Pemilik::search($search)
            ->latest()
            ->paginate(5);

        return view('app.pemiliks.index', compact('pemiliks', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Pemilik::class);

        return view('app.pemiliks.create');
    }

    /**
     * @param \App\Http\Requests\PemilikStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PemilikStoreRequest $request)
    {
        $this->authorize('create', Pemilik::class);

        $validated = $request->validated();

        $pemilik = Pemilik::create($validated);

        return redirect()
            ->route('pemiliks.edit', $pemilik)
            ->withSuccess(__('crud.common.created'));
    }

    public function generate(Pemilik $pemilik)
    {
        if($pemilik->email == null) {
            return redirect()
                ->route('pemiliks.index')
                ->withErrors('Pemilik belum mengisi email');
        }

        $pemilik = User::create([
            'name' => $pemilik->nama,
            'email' => $pemilik->email,
            'password' => Hash::make($pemilik->nik),
        ]);

        $pemilik->assignRole('pemilik');

        return redirect()
            ->route('pemiliks.index')
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pemilik $pemilik
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Pemilik $pemilik)
    {
        $this->authorize('view', $pemilik);

        return view('app.pemiliks.show', compact('pemilik'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pemilik $pemilik
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Pemilik $pemilik)
    {
        $this->authorize('update', $pemilik);

        return view('app.pemiliks.edit', compact('pemilik'));
    }

    /**
     * @param \App\Http\Requests\PemilikUpdateRequest $request
     * @param \App\Models\Pemilik $pemilik
     * @return \Illuminate\Http\Response
     */
    public function update(PemilikUpdateRequest $request, Pemilik $pemilik)
    {
        $this->authorize('update', $pemilik);

        $validated = $request->validated();

        $pemilik->update($validated);

        return redirect()
            ->route('pemiliks.edit', $pemilik)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pemilik $pemilik
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Pemilik $pemilik)
    {
        $this->authorize('delete', $pemilik);

        $pemilik->delete();

        return redirect()
            ->route('pemiliks.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
