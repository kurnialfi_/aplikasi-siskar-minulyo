<?php

namespace App\Http\Controllers;

use App\Models\Komoditi;
use Illuminate\Http\Request;
use App\Http\Requests\KomoditiStoreRequest;
use App\Http\Requests\KomoditiUpdateRequest;

class KomoditiController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Komoditi::class);

        $search = $request->get('search', '');

        $komoditis = Komoditi::search($search)
            ->latest()
            ->paginate(5);

        return view('app.komoditis.index', compact('komoditis', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Komoditi::class);

        return view('app.komoditis.create');
    }

    /**
     * @param \App\Http\Requests\KomoditiStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(KomoditiStoreRequest $request)
    {
        $this->authorize('create', Komoditi::class);

        $validated = $request->validated();

        $komoditi = Komoditi::create($validated);

        return redirect()
            ->route('komoditis.edit', $komoditi)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Komoditi $komoditi
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Komoditi $komoditi)
    {
        $this->authorize('view', $komoditi);

        return view('app.komoditis.show', compact('komoditi'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Komoditi $komoditi
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Komoditi $komoditi)
    {
        $this->authorize('update', $komoditi);

        return view('app.komoditis.edit', compact('komoditi'));
    }

    /**
     * @param \App\Http\Requests\KomoditiUpdateRequest $request
     * @param \App\Models\Komoditi $komoditi
     * @return \Illuminate\Http\Response
     */
    public function update(KomoditiUpdateRequest $request, Komoditi $komoditi)
    {
        $this->authorize('update', $komoditi);

        $validated = $request->validated();

        $komoditi->update($validated);

        return redirect()
            ->route('komoditis.edit', $komoditi)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Komoditi $komoditi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Komoditi $komoditi)
    {
        $this->authorize('delete', $komoditi);

        $komoditi->delete();

        return redirect()
            ->route('komoditis.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
