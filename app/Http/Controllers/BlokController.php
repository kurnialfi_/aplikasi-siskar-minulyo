<?php

namespace App\Http\Controllers;

use App\Models\Blok;
use Illuminate\Http\Request;
use App\Http\Requests\BlokStoreRequest;
use App\Http\Requests\BlokUpdateRequest;

class BlokController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Blok::class);

        $search = $request->get('search', '');

        $bloks = Blok::search($search)
            ->latest()
            ->paginate(5);

        return view('app.bloks.index', compact('bloks', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Blok::class);

        return view('app.bloks.create');
    }

    /**
     * @param \App\Http\Requests\BlokStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlokStoreRequest $request)
    {
        $this->authorize('create', Blok::class);

        $validated = $request->validated();

        $blok = Blok::create($validated);

        return redirect()
            ->route('bloks.edit', $blok)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Blok $blok
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Blok $blok)
    {
        $this->authorize('view', $blok);

        return view('app.bloks.show', compact('blok'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Blok $blok
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Blok $blok)
    {
        $this->authorize('update', $blok);

        return view('app.bloks.edit', compact('blok'));
    }

    /**
     * @param \App\Http\Requests\BlokUpdateRequest $request
     * @param \App\Models\Blok $blok
     * @return \Illuminate\Http\Response
     */
    public function update(BlokUpdateRequest $request, Blok $blok)
    {
        $this->authorize('update', $blok);

        $validated = $request->validated();

        $blok->update($validated);

        return redirect()
            ->route('bloks.edit', $blok)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Blok $blok
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Blok $blok)
    {
        $this->authorize('delete', $blok);

        $blok->delete();

        return redirect()
            ->route('bloks.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
