<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Blok;
use App\Models\Toko;
use App\Models\Komoditi;
use App\Models\DetailTokoKomoditi;
use Illuminate\Http\Request;

class RuleController extends Controller
{
    public function index(Type $var = null)
    {
        $tokos = Toko::all();

        return view('app.rule.index', compact('tokos'));
    }

    public function toko(Request $request)
    {
        $toko = Toko::find($request->id);

        $blok = Blok::find($toko->blok_id);

        $detailKomoditi = DB::select("
            select b.nama from detail_toko_komoditi a join detail_komoditi b on b.id = a.detail_komoditi_id
            where a.toko_id = '$toko->id'
        ");

        $detailKomoditi = collect($detailKomoditi);

        return response()->json([
            'view' => view('app.rule._detail',[
                'toko' => $toko,
                'blok' => $blok,
                'detailKomoditi' => $detailKomoditi
            ])->render()
        ]);
    }
}
