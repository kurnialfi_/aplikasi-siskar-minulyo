<?php

namespace App\Http\Controllers;

use App\Models\Pengunjung;
use App\Models\DetailToko;
use Illuminate\Http\Request;
use App\Http\Requests\PengunjungStoreRequest;
use App\Http\Requests\PengunjungUpdateRequest;

class PengunjungController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Pengunjung::class);

        $search = $request->get('search', '');

        $pengunjungs = Pengunjung::search($search)
            ->latest()
            ->paginate(5);

        return view('app.pengunjungs.index', compact('pengunjungs', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Pengunjung::class);

        $detailTokos = DetailToko::pluck('toko_id', 'id');

        return view('app.pengunjungs.create', compact('detailTokos'));
    }

    /**
     * @param \App\Http\Requests\PengunjungStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengunjungStoreRequest $request)
    {
        $this->authorize('create', Pengunjung::class);

        $validated = $request->validated();

        $pengunjung = Pengunjung::create($validated);

        return redirect()
            ->route('pengunjungs.edit', $pengunjung)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pengunjung $pengunjung
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Pengunjung $pengunjung)
    {
        $this->authorize('view', $pengunjung);

        return view('app.pengunjungs.show', compact('pengunjung'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pengunjung $pengunjung
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Pengunjung $pengunjung)
    {
        $this->authorize('update', $pengunjung);

        $detailTokos = DetailToko::pluck('toko_id', 'id');

        return view(
            'app.pengunjungs.edit',
            compact('pengunjung', 'detailTokos')
        );
    }

    /**
     * @param \App\Http\Requests\PengunjungUpdateRequest $request
     * @param \App\Models\Pengunjung $pengunjung
     * @return \Illuminate\Http\Response
     */
    public function update(
        PengunjungUpdateRequest $request,
        Pengunjung $pengunjung
    ) {
        $this->authorize('update', $pengunjung);

        $validated = $request->validated();

        $pengunjung->update($validated);

        return redirect()
            ->route('pengunjungs.edit', $pengunjung)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Pengunjung $pengunjung
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Pengunjung $pengunjung)
    {
        $this->authorize('delete', $pengunjung);

        $pengunjung->delete();

        return redirect()
            ->route('pengunjungs.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
