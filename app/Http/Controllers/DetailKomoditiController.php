<?php

namespace App\Http\Controllers;

use App\Models\Komoditi;
use Illuminate\Http\Request;
use App\Models\DetailKomoditi;
use App\Http\Requests\DetailKomoditiStoreRequest;
use App\Http\Requests\DetailKomoditiUpdateRequest;

class DetailKomoditiController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', DetailKomoditi::class);

        $search = $request->get('search', '');

        $detailKomoditis = DetailKomoditi::search($search)
            ->latest()
            ->paginate(5);

        return view(
            'app.detail_komoditis.index',
            compact('detailKomoditis', 'search')
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', DetailKomoditi::class);

        $komoditis = Komoditi::pluck('nama', 'id');

        return view('app.detail_komoditis.create', compact('komoditis'));
    }

    /**
     * @param \App\Http\Requests\DetailKomoditiStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DetailKomoditiStoreRequest $request)
    {
        $this->authorize('create', DetailKomoditi::class);

        $validated = $request->validated();

        $detailKomoditi = DetailKomoditi::create($validated);

        return redirect()
            ->route('detail-komoditis.edit', $detailKomoditi)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailKomoditi $detailKomoditi
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, DetailKomoditi $detailKomoditi)
    {
        $this->authorize('view', $detailKomoditi);

        return view('app.detail_komoditis.show', compact('detailKomoditi'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailKomoditi $detailKomoditi
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, DetailKomoditi $detailKomoditi)
    {
        $this->authorize('update', $detailKomoditi);

        $komoditis = Komoditi::pluck('nama', 'id');

        return view(
            'app.detail_komoditis.edit',
            compact('detailKomoditi', 'komoditis')
        );
    }

    /**
     * @param \App\Http\Requests\DetailKomoditiUpdateRequest $request
     * @param \App\Models\DetailKomoditi $detailKomoditi
     * @return \Illuminate\Http\Response
     */
    public function update(
        DetailKomoditiUpdateRequest $request,
        DetailKomoditi $detailKomoditi
    ) {
        $this->authorize('update', $detailKomoditi);

        $validated = $request->validated();

        $detailKomoditi->update($validated);

        return redirect()
            ->route('detail-komoditis.edit', $detailKomoditi)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DetailKomoditi $detailKomoditi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, DetailKomoditi $detailKomoditi)
    {
        $this->authorize('delete', $detailKomoditi);

        $detailKomoditi->delete();

        return redirect()
            ->route('detail-komoditis.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
