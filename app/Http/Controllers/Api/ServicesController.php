<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\Komoditi;
use App\Models\Toko;
use App\Models\DetailKomoditi;
use App\Models\DetailTokoKomoditi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /** for android **/
    public function getKomoditi()
    {
        $komoditi = Komoditi::all();
        $code = 200;
        
        return response()->json([
            'data' => $komoditi
        ], $code);
    }
    
    /** for android **/
    public function getKomoditiDetail(Request $request)
    {
        $komoditi = DetailKomoditi::where('komoditi_id', $request->id)->get();
        
        $code = 200;
        
        return response()->json([
            'data' => $komoditi
        ], $code);
    }
    
    /** for android **/
    
    public function getDetailKomoditi(Request $request)
    {
        $toko = Toko::where('id', $request->id)->first();
        
        $detailKomoditi = DetailKomoditi::where('komoditi_id', $toko->komoditi_id)->get();

        if(!$request->filled('action')) {
    
            $checkBoxView =  "";
    
            foreach ($detailKomoditi as $key => $komoditi) {
                    // $checkBoxView .= component('checkbox')
                $checkBoxView .= "
                    <div class'form-check'>
                        <input type='checkbox' class='form-check-input' id='komoditi[{$komoditi->id}]' name='komoditi[]' value='{$komoditi->id}'>
                        <label class='form-check-label' for='komoditi[{$komoditi->id}]'>
                            {$komoditi->nama}
                        </label>
                    </div>
                ";
            }
    
            $checkBoxView .= " ";
        } else {
            $detailTokoKomoditi = DetailTokoKomoditi::where('detail_toko_id', $request->detail_toko_id)->pluck('detail_komoditi_id')->toArray();

            $checkBoxView =  "";
    
            foreach ($detailKomoditi as $key => $komoditi) {
                    // $checkBoxView .= component('checkbox')
                $checked = '';

                if(in_array($komoditi->id, $detailTokoKomoditi)) {
                    $checked = 'checked';
                }

                $checkBoxView .= "
                    <div class'form-check'>
                        <input type='checkbox' class='form-check-input' id='komoditi[{$komoditi->id}]' name='komoditi[]' {$checked} value='{$komoditi->id}'>
                        <label class='form-check-label' for='komoditi[{$komoditi->id}]'>
                            {$komoditi->nama}
                        </label>
                    </div>
                ";
            }
    
            $checkBoxView .= " ";
        }

        return response()->json([
            'view' => $checkBoxView
        ]);
    }

    public function searchKomoditi(Request $request)
    {   
        $code = 200;
            $message = 'Succes';
            DB::enableQueryLog();
            
            $freshString = str_replace("]", "",$request->detail_komoditi);
            $freshString = str_replace("[", "",$freshString);
            // clear space
            $freshString = str_replace(" ", "", $freshString);
            $exploded = explode(",",$freshString);
            
            $komoditi = DB::table('detail_toko_komoditi as a')
                            ->leftJoin('detail_toko as b', 'b.id', '=', 'a.detail_toko_id')
                            ->join('pemilik as d', 'd.id', '=', 'b.pemilik_id')
                            ->select([
                                'a.toko_id',
                                'a.detail_komoditi_id',
                                'a.detail_toko_id',
                                'b.profill_toko',
                                'b.deskripsi',
                                'd.nama',
                                'd.alamat',
                                'd.no_telp',
                                DB::raw('(select c.blok_id from toko c where c.id = a.toko_id) as blok_id'),
                                DB::raw('group_concat(a.komoditi) as komoditi'),
                                DB::raw('(select avg(e.rating) from detail_toko_ratings e where e.detail_toko_id = a.detail_toko_id) as ratings')
                            ])
                            ->groupBy('a.toko_id')
                            ->whereIn('a.detail_komoditi_id', $exploded)
                            ->get();
            $komoditi->map(function($row){
                $row->detail_toko_id = (int) $row->detail_toko_id;
                $row->ratings = number_format($row->ratings, 1);
                return $row;
            });
            
            if(count($komoditi) == 0) {
                $code = 404;
                $message = 'Tidak ditemukan';
            }

        return response()->json([
            'code' => $code,
            'message' => $message,
            'data' => $komoditi
        ], $code);


    }

    public function getDetailToko($toko, Request $request)
    {
        
        
        $device_id = $request->filled('device_id') ? $request->device_id : '0';

        $toko = DB::select("
            select
            	a.toko_id,
            	a.id,
            	( 
            		select 
            			e.nama
            		from toko d
            		join komoditi e
            			on e.id = d.komoditi_id
            		where d.id = a.toko_id
            	) as komoditi,
            	( SELECT group_concat( b.komoditi ) FROM detail_toko_komoditi b WHERE b.detail_toko_id = a.id ) as detail_komoditi,
            	a.deskripsi,
            	a.profill_toko,
            	e.nama as pemilik,
            	(
            	select
            	if
            		( count(*) <> 0, 'true', 'false' ) 
            	from
            		detail_toko_ratings c 
            	where
            		c.detail_toko_id = a.id 
            		and c.device_id = '{$device_id}' 
            	) as rating 
            from
            	detail_toko a 
            join pemilik e
            	on e.id = a.pemilik_id
            where
            	a.id = $toko
        ");

        $data = collect($toko)->first();

        return response()->json([
            'data' => $data
        ]);

    }

    public function getTokoRating(Request $request)
    {
        $ratings = DB::select("
            select * from detail_toko_ratings where detail_toko_id = $request->id
        ");
        
        $ratings = collect($ratings);
        
        $ratings->map(function($row){
                $row->rating = (int) $row->rating;
                return $row;
            });

        return response()->json([
            'data' => $ratings
        ]);
    }

    public function storeRating(Request $request)
    {
        DB::beginTransaction();

        try {
            DB::table('detail_toko_ratings')
                ->insert([
                    'detail_toko_id' => $request->detail_toko_id,
                    'device_id' => $request->device_id,
                    'rating' => $request->rating,
                    'comment' => $request->comment
                ]);
            DB::commit();

            $code = 200;
            $message = 'Ok';
        } catch (\Exception $ex) {
            DB::rollback();
            $code = 500;
            $message = 'Failed : ' . $ex->getMessage();
        }

        return response()->json(['code' => $code, 'message' => $message, 'error' => [
                    'detail_toko_id' => $request->detail_toko_id,
                    'device_id' => $request->device_id,
                    'rating' => $request->rating,
                    'comment' => $request->comment
                ]], $code);
    }
}
