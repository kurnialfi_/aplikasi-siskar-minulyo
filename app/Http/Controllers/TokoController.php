<?php

namespace App\Http\Controllers;

use App\Models\Toko;
use App\Models\Blok;
use App\Models\Komoditi;
use Illuminate\Http\Request;
use App\Http\Requests\TokoStoreRequest;
use App\Http\Requests\TokoUpdateRequest;

class TokoController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Toko::class);

        $search = $request->get('search', '');

        $tokos = Toko::search($search)
            ->latest()
            ->paginate(5);

        return view('app.tokos.index', compact('tokos', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Toko::class);

        $bloks = Blok::pluck('id', 'id');
        $komoditis = Komoditi::pluck('nama', 'id');

        return view('app.tokos.create', compact('bloks', 'komoditis'));
    }

    /**
     * @param \App\Http\Requests\TokoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TokoStoreRequest $request)
    {
        $this->authorize('create', Toko::class);

        $validated = $request->validated();

        $toko = Toko::create($validated);

        return redirect()
            ->route('tokos.edit', $toko)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Toko $toko
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Toko $toko)
    {
        $this->authorize('view', $toko);

        return view('app.tokos.show', compact('toko'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Toko $toko
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Toko $toko)
    {
        $this->authorize('update', $toko);

        $bloks = Blok::pluck('id', 'id');
        $komoditis = Komoditi::pluck('nama', 'id');

        return view('app.tokos.edit', compact('toko', 'bloks', 'komoditis'));
    }

    /**
     * @param \App\Http\Requests\TokoUpdateRequest $request
     * @param \App\Models\Toko $toko
     * @return \Illuminate\Http\Response
     */
    public function update(TokoUpdateRequest $request, Toko $toko)
    {
        $this->authorize('update', $toko);

        $validated = $request->validated();

        $toko->update($validated);

        return redirect()
            ->route('tokos.edit', $toko)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Toko $toko
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Toko $toko)
    {
        $this->authorize('delete', $toko);

        $toko->delete();

        return redirect()
            ->route('tokos.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
