<?php

namespace App\Http\Controllers\Pemilik;

use DB;
use Auth;
use App\Models\Rating;
use App\Models\DetailToko;
use App\Models\Pemilik;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TokoController extends Controller
{
    public function index()
    {
        $pemilik = Pemilik::where('email', Auth::user()->email)->first();

        $toko = DetailToko::where('pemilik_id', $pemilik->id)->first();

        $rating = [];

        $ratings = DB::select("
            select * from detail_toko_ratings where detail_toko_id = $toko->id
        ");

        $ratings = collect($ratings);
        
        $ratings->map(function($row) {
            $row->rating = (int) $row->rating;
            return $row;
        });

        $rating = $ratings->pluck('rating')->toArray();

        $rating = count($rating) != 0 ? number_format(array_sum($rating) / count($rating), 1) : null;

        return view('app.pemilik_toko.index', compact('pemilik', 'toko', 'ratings', 'rating'));
    }
}
