<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class Blok extends Model
{
    use Searchable;

    protected $fillable = ['nama','id'];

    protected $searchableFields = ['*'];

    protected $table = 'blok';

    public $incrementing = false;

    public function tokos()
    {
        return $this->hasMany(Toko::class);
    }
}
