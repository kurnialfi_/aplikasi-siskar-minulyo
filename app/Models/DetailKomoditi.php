<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class DetailKomoditi extends Model
{
    use Searchable;

    protected $fillable = ['komoditi_id', 'nama'];

    protected $searchableFields = ['*'];

    protected $table = 'detail_komoditi';

    public function komoditi()
    {
        return $this->belongsTo(Komoditi::class);
    }

    public function detailTokos()
    {
        return $this->hasMany(DetailToko::class);
    }
}
