<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class Pemilik extends Model
{
    use Searchable;

    protected $fillable = ['nik', 'nama', 'alamat', 'no_telp', 'email'];

    protected $searchableFields = ['*'];

    protected $table = 'pemilik';

    public function PemilikdetailTokos()
    {
        return $this->hasMany(DetailToko::class);
    }
}
