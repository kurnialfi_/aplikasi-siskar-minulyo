<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class Pengunjung extends Model
{
    use Searchable;

    protected $fillable = ['detail_toko_id', 'nama', 'komentar', 'rating'];

    protected $searchableFields = ['*'];

    protected $table = 'pengunjung';

    public function detailToko()
    {
        return $this->belongsTo(DetailToko::class);
    }
}
