<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
    use Searchable;

    protected $fillable = [
        'id',
        'blok_id',
        'komoditi_id',
        'no_toko',
        'longitude',
        'latitude',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'toko';

    public $incrementing = false;

    public function blok()
    {
        return $this->belongsTo(Blok::class);
    }

    public function detailTokos()
    {
        return $this->hasMany(DetailToko::class);
    }

    public function komoditi()
    {
        return $this->belongsTo(Komoditi::class);
    }
}
