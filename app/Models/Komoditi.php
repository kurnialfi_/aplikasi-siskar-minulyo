<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class Komoditi extends Model
{
    use Searchable;

    protected $fillable = ['nama'];

    protected $searchableFields = ['*'];

    protected $table = 'komoditi';

    public function detailKomoditis()
    {
        return $this->hasMany(DetailKomoditi::class);
    }

    public function komoditi_toko()
    {
        return $this->hasMany(Toko::class);
    }
}
