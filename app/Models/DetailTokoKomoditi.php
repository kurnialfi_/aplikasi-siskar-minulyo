<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetailTokoKomoditi extends Model
{
    protected $table = 'detail_toko_komoditi';
    public $guarded  = [];
}
