<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;

class DetailToko extends Model
{
    use Searchable;

    protected $fillable = [
        'toko_id',
        'pemilik_id',
        'detail_komoditi_id',
        'profill_toko',
        'deskripsi',
    ];

    protected $searchableFields = ['*'];

    protected $table = 'detail_toko';

    public function toko()
    {
        return $this->belongsTo(Toko::class);
    }

    public function detailKomoditi()
    {
        return $this->belongsTo(DetailKomoditi::class);
    }

    public function pemilik()
    {
        return $this->belongsTo(Pemilik::class);
    }

    public function pengunjungs()
    {
        return $this->hasMany(Pengunjung::class);
    }
}
