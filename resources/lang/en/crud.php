<?php

return [
    'common' => [
        'actions' => 'Actions',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'new' => 'New',
        'cancel' => 'Cancel',
        'save' => 'Save',
        'delete' => 'Delete',
        'delete_selected' => 'Delete selected',
        'search' => 'Search...',
        'back' => 'Back to Index',
        'are_you_sure' => 'Are you sure?',
        'no_items_found' => 'No items found',
        'created' => 'Successfully created',
        'saved' => 'Saved successfully',
        'removed' => 'Successfully removed',
    ],

    'blok' => [
        'name' => 'Blok',
        'index_title' => 'Blok List',
        'new_title' => 'New Blok',
        'create_title' => 'Create Blok',
        'edit_title' => 'Edit Blok',
        'show_title' => 'Show Blok',
        'inputs' => [
            'id' => 'Id',
            'nama' => 'Nama',
        ],
    ],

    'komoditi' => [
        'name' => 'Komoditi',
        'index_title' => 'Komoditi List',
        'new_title' => 'New Komoditi',
        'create_title' => 'Create Komoditi',
        'edit_title' => 'Edit Komoditi',
        'show_title' => 'Show Komoditi',
        'inputs' => [
            'nama' => 'Nama',
        ],
    ],

    'detail_komoditi' => [
        'name' => 'Detail Komoditi',
        'index_title' => 'Detail Komoditi List',
        'new_title' => 'New Detail komoditi',
        'create_title' => 'Create Detail Komoditi',
        'edit_title' => 'Edit Detail Komoditi',
        'show_title' => 'Show Detail Komoditi',
        'inputs' => [
            'komoditi_id' => 'Komoditi',
            'nama' => 'Nama',
        ],
    ],

    'toko' => [
        'name' => 'Toko',
        'index_title' => 'Toko List',
        'new_title' => 'New Toko',
        'create_title' => 'Create Toko',
        'edit_title' => 'Edit Toko',
        'show_title' => 'Show Toko',
        'inputs' => [
            'id' => 'Id',
            'blok_id' => 'Blok',
            'no_toko' => 'No Toko',
            'komoditi_id' => 'Komoditi',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
        ],
    ],

    'detail_toko' => [
        'name' => 'Detail Toko',
        'index_title' => 'Detail Toko List',
        'new_title' => 'New Detail toko',
        'create_title' => 'Create Detail Toko',
        'edit_title' => 'Edit Detail Toko',
        'show_title' => 'Show Detail Toko',
        'inputs' => [
            'toko_id' => 'Toko',
            'detail_komoditi_id' => 'Detail Komoditi',
            'pemilik_id' => 'Pemilik',
            'profill_toko' => 'Profill Toko',
            'deskripsi' => 'Deskripsi',
        ],
    ],

    'pemilik' => [
        'name' => 'Pemilik',
        'index_title' => 'Pemilik List',
        'new_title' => 'New Pemilik',
        'create_title' => 'Create Pemilik',
        'edit_title' => 'Edit Pemilik',
        'show_title' => 'Show Pemilik',
        'inputs' => [
            'nik' => 'Nik',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'no_telp' => 'No Telp',
            'email' => 'Email'
        ],
    ],

    'pengunjung' => [
        'name' => 'Pengunjung',
        'index_title' => 'Pengunjung List',
        'new_title' => 'New Pengunjung',
        'create_title' => 'Create Pengunjung',
        'edit_title' => 'Edit Pengunjung',
        'show_title' => 'Show Pengunjung',
        'inputs' => [
            'detail_toko_id' => 'Detail Toko',
            'nama' => 'Nama',
            'komentar' => 'Komentar',
            'rating' => 'Rating',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
