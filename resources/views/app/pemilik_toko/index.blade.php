@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-body">
              <h4 class="card-title">
                  <a href="{{ route('home') }}" class="mr-4"
                      ><i class="icon ion-md-arrow-back"></i
                  ></a>
                  Identitas
              </h4>
  
              <div class="mt-4">
                  <div class="mb-4">
                      <h5>@lang('crud.pemilik.inputs.nik')</h5>
                      <span>{{ $pemilik->nik ?? '-' }}</span>
                  </div>
                  <div class="mb-4">
                      <h5>@lang('crud.pemilik.inputs.nama')</h5>
                      <span>{{ $pemilik->nama ?? '-' }}</span>
                  </div>
                  <div class="mb-4">
                      <h5>@lang('crud.pemilik.inputs.alamat')</h5>
                      <span>{{ $pemilik->alamat ?? '-' }}</span>
                  </div>
                  <div class="mb-4">
                      <h5>@lang('crud.pemilik.inputs.no_telp')</h5>
                      <span>{{ $pemilik->no_telp ?? '-' }}</span>
                  </div>
              </div>
          </div>
        </div>
      </div>
      <div class="col-md-7">
        @if ($toko == null)
          <div class="card card-danger">
              <div class="card-body">
                  <h5>Anda belum memiliki toko terdaftar</h5>
              </div>
          </div>
        @else
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">
                    Informasi Toko
                </h4>

                {{-- {{ dd($toko) }} --}}
                <div class="mt-4">
                    <div class="mb-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h5>@lang('crud.detail_toko.inputs.toko_id')</h5>
                                <span>{{ optional($toko->toko)->id ?? '-' }}</span>
                            </div>
                            <div class="col-md-6">
                                <h5>Ulasan</h5>
                                <span><i class="icon ion-md-star"></i> {{ $rating ?? 'Belum ada' }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-4">
                        <h5>@lang('crud.detail_toko.inputs.detail_komoditi_id')</h5>
                        <span
                            >{{ $toko->komoditi ?? '-' }}</span
                        >
                    </div>
                    <div class="mb-4">
                        <h5>@lang('crud.detail_toko.inputs.pemilik_id')</h5>
                        <span
                            >{{ optional($toko->pemilik)->nik ?? '-' }} ({{ optional($toko->pemilik)->nama ?? '-' }})</span
                        >
                    </div>
                    <div class="mb-4">
                        <h5>@lang('crud.detail_toko.inputs.profill_toko')</h5>
                        <x-partials.thumbnail
                            src="{{ $toko->profill_toko ? \Storage::url($toko->profill_toko) : '' }}"
                            size="150"
                        />
                    </div>
                    <div class="mb-4">
                        <h5>@lang('crud.detail_toko.inputs.deskripsi')</h5>
                        <span>{{ $toko->deskripsi ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        {{-- {{ dd($ratings) }} --}}
                        <h5>Ulasan pengunjung</h5>
                        <table class="table table-stripped">
                            @if (count($ratings) != 0)
                                @foreach ($ratings as $key => $item)
                                <tr>
                                    <td>
                                        <i class="icon ion-md-star"></i> {{ $item->rating }}<br>
                                        {{ $item->comment }}
                                    </td>
                                </tr>
                                @endforeach
                            @else 
                                <tr>
                                    <td class="text-center">Belum memiliki ulasan</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
      </div>
    </div>
</div>
@endsection
