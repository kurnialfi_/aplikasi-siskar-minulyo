@php $editing = isset($pengunjung) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="detail_toko_id" label="Detail Toko" required>
            @php $selected = old('detail_toko_id', ($editing ? $pengunjung->detail_toko_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Detail Toko</option>
            @foreach($detailTokos as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="nama"
            label="Nama"
            value="{{ old('nama', ($editing ? $pengunjung->nama : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="komentar"
            label="Komentar"
            value="{{ old('komentar', ($editing ? $pengunjung->komentar : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="rating" label="Rating">
            @php $selected = old('rating', ($editing ? $pengunjung->rating : '')) @endphp
            <option value="1" {{ $selected == '1' ? 'selected' : '' }} >1</option>
            <option value="2" {{ $selected == '2' ? 'selected' : '' }} >2</option>
            <option value="3" {{ $selected == '3' ? 'selected' : '' }} >3</option>
            <option value="4" {{ $selected == '4' ? 'selected' : '' }} >4</option>
            <option value="5" {{ $selected == '5' ? 'selected' : '' }} >5</option>
        </x-inputs.select>
    </x-inputs.group>
</div>
