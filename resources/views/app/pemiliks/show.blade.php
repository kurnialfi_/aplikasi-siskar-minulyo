@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('pemiliks.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.pemilik.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.pemilik.inputs.nik')</h5>
                    <span>{{ $pemilik->nik ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.pemilik.inputs.nama')</h5>
                    <span>{{ $pemilik->nama ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.pemilik.inputs.alamat')</h5>
                    <span>{{ $pemilik->alamat ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.pemilik.inputs.no_telp')</h5>
                    <span>{{ $pemilik->no_telp ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('pemiliks.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Pemilik::class)
                <a href="{{ route('pemiliks.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
