@php $editing = isset($pemilik) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12 col-lg-5">
        <x-inputs.text
            name="nik"
            label="Nik"
            value="{{ old('nik', ($editing ? $pemilik->nik : '')) }}"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-7">
        <x-inputs.text
            name="nama"
            label="Nama"
            value="{{ old('nama', ($editing ? $pemilik->nama : '')) }}"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="alamat" label="Alamat" required
            >{{ old('alamat', ($editing ? $pemilik->alamat : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.text
            name="no_telp"
            label="No Telp"
            value="{{ old('no_telp', ($editing ? $pemilik->no_telp : '')) }}"
            required
        ></x-inputs.text>
    </x-inputs.group>
    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.email
            name="email"
            label="Email"
            value="{{ old('email', ($editing ? $pemilik->email : '')) }}"
            required
        ></x-inputs.email>
    </x-inputs.group>
</div>
