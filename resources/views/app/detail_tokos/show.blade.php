@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('detail-tokos.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.detail_toko.show_title')
            </h4>

            {{-- {{ dd($detailToko) }} --}}
            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.detail_toko.inputs.toko_id')</h5>
                    <span>{{ optional($detailToko->toko)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.detail_toko.inputs.detail_komoditi_id')</h5>
                    <span
                        >{{ $detailToko->komoditi ?? '-' }}</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.detail_toko.inputs.pemilik_id')</h5>
                    <span
                        >{{ optional($detailToko->pemilik)->nik ?? '-' }} ({{ optional($detailToko->pemilik)->nama ?? '-' }})</span
                    >
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.detail_toko.inputs.profill_toko')</h5>
                    <x-partials.thumbnail
                        src="{{ $detailToko->profill_toko ? \Storage::url($detailToko->profill_toko) : '' }}"
                        size="150"
                    />
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.detail_toko.inputs.deskripsi')</h5>
                    <span>{{ $detailToko->deskripsi ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a
                    href="{{ route('detail-tokos.index') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\DetailToko::class)
                <a
                    href="{{ route('detail-tokos.create') }}"
                    class="btn btn-light"
                >
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
