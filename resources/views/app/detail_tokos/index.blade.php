@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <div style="display: flex; justify-content: space-between;">
                <h4 class="card-title">
                    @lang('crud.detail_toko.index_title')
                </h4>
            </div>

            <div class="searchbar mt-4 mb-5">
                <div class="row">
                    <div class="col-md-6">
                        <form>
                            <div class="input-group">
                                <input
                                    id="indexSearch"
                                    type="text"
                                    name="search"
                                    placeholder="{{ __('crud.common.search') }}"
                                    value="{{ $search ?? '' }}"
                                    class="form-control"
                                    autocomplete="off"
                                />
                                <div class="input-group-append">
                                    <button
                                        type="submit"
                                        class="btn btn-primary"
                                    >
                                        <i class="icon ion-md-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6 text-right">
                        @can('create', App\Models\DetailToko::class)
                        <a
                            href="{{ route('detail-tokos.create') }}"
                            class="btn btn-primary"
                        >
                            <i class="icon ion-md-add"></i>
                            @lang('crud.common.create')
                        </a>
                        @endcan
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-borderless table-hover">
                    <thead>
                        <tr>
                            <th class="text-left">
                                @lang('crud.detail_toko.inputs.toko_id')
                            </th>
                            <th class="text-left">
                                Komoditi
                            </th>                            
                            <th class="text-left">
                                @lang('crud.detail_toko.inputs.detail_komoditi_id')
                            </th>
                            <th class="text-left">
                                @lang('crud.detail_toko.inputs.pemilik_id')
                            </th>
                            <th class="text-left">
                                Profile Toko
                            </th>
                            <th class="text-left">
                                @lang('crud.detail_toko.inputs.deskripsi')
                            </th>
                            <th class="text-center">
                                @lang('crud.common.actions')
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($detailTokos as $detailToko)
                        <tr>
                            <td>
                                {{ optional($detailToko->toko)->id ?? '-' }}
                            </td>
                            <td>
                                {{ optional($detailToko->toko->komoditi)->nama ?? '-' }}
                            </td>                            
                            <td>
                                {{ \Str::limit($detailToko->komoditi, 25)
                                ?? '-' }}
                            </td>
                            <td>
                                {{ optional($detailToko->pemilik)->nik ?? '-' }} ( {{ optional($detailToko->pemilik)->nama ?? '-' }} )
                            </td>
                            <td>
                                <x-partials.thumbnail
                                    src="{{ $detailToko->profill_toko ? \Storage::url($detailToko->profill_toko) : '' }}"
                                />
                            </td>
                            <td>{{ $detailToko->deskripsi ?? '-' }}</td>
                            <td class="text-center" style="width: 134px;">
                                <div
                                    role="group"
                                    aria-label="Row Actions"
                                    class="btn-group"
                                >
                                    @can('update', $detailToko)
                                    <a
                                        href="{{ route('detail-tokos.edit', $detailToko) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-create"></i>
                                        </button>
                                    </a>
                                    @endcan @can('view', $detailToko)
                                    <a
                                        href="{{ route('detail-tokos.show', $detailToko) }}"
                                    >
                                        <button
                                            type="button"
                                            class="btn btn-light"
                                        >
                                            <i class="icon ion-md-eye"></i>
                                        </button>
                                    </a>
                                    @endcan @can('delete', $detailToko)
                                    <form
                                        action="{{ route('detail-tokos.destroy', $detailToko) }}"
                                        method="POST"
                                        onsubmit="return confirm('{{ __('crud.common.are_you_sure') }}')"
                                    >
                                        @csrf @method('DELETE')
                                        <button
                                            type="submit"
                                            class="btn btn-light text-danger"
                                        >
                                            <i class="icon ion-md-trash"></i>
                                        </button>
                                    </form>
                                    @endcan
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">
                                @lang('crud.common.no_items_found')
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">{!! $detailTokos->render() !!}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
