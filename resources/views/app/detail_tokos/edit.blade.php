@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('detail-tokos.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.detail_toko.edit_title')
            </h4>

            <x-form
                method="PUT"
                action="{{ route('detail-tokos.update', $detailToko) }}"
                has-files
                class="mt-4"
            >
                @include('app.detail_tokos.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('detail-tokos.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <a
                        href="{{ route('detail-tokos.create') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-add text-primary"></i>
                        @lang('crud.common.create')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.update')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection

@section('js-script')
    <script>
        $(function() {
            $.get('{{ route('api.services.detail-komoditi') }}', {id: '{{ $detailToko->toko_id }}', detail_toko_id: {{ $detailToko->id }}, action: 'edit' }, function(response){
                $('#komoditi-container').html(response.view)
            })  
        })
    </script>
@endsection
