@php $editing = isset($detailToko) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="toko_id" label="Toko" required>
            @php $selected = old('toko_id', ($editing ? $detailToko->toko_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Toko</option>
            @foreach($tokos as $value => $label)
            <option value="{{ $label->id }}" {{ $selected == $label->id ? 'selected' : '' }} >{{ $label->id }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        @php
            $label = 'Detail Komoditi';
            $name = $label;
        @endphp
        @include('components.inputs.partials.label')
        <div id="komoditi-container" class="form-group"></div>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-8">
        <x-inputs.select name="pemilik_id" label="Pemilik" required>
            @php $selected = old('pemilik_id', ($editing ? $detailToko->pemilik_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Pemilik</option>
            @foreach($pemiliks as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-4">
        <div
            x-data="imageViewer('{{ $editing && $detailToko->profill_toko ? \Storage::url($detailToko->profill_toko) : '' }}')"
        >
            <x-inputs.partials.label
                name="profill_toko"
                label="Profill Toko"
            ></x-inputs.partials.label
            ><br />

            <!-- Show the image -->
            <template x-if="imageUrl">
                <img
                    :src="imageUrl"
                    class="object-cover rounded border border-gray-200"
                    style="width: 100px; height: 100px;"
                />
            </template>

            <!-- Show the gray box when image is not available -->
            <template x-if="!imageUrl">
                <div
                    class="border rounded border-gray-200 bg-gray-100"
                    style="width: 100px; height: 100px;"
                ></div>
            </template>

            <div class="mt-2">
                <input
                    type="file"
                    name="profill_toko"
                    id="profill_toko"
                    @change="fileChosen"
                />
            </div>

            @error('profill_toko') @include('components.inputs.partials.error')
            @enderror
        </div>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea name="deskripsi" label="Deskripsi" required
            >{{ old('deskripsi', ($editing ? $detailToko->deskripsi : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>
</div>
