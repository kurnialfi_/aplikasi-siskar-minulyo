@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('detail-tokos.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.detail_toko.create_title')
            </h4>

            <x-form
                method="POST"
                action="{{ route('detail-tokos.store') }}"
                has-files
                class="mt-4"
            >
                @include('app.detail_tokos.form-inputs')

                <div class="mt-4">
                    <a
                        href="{{ route('detail-tokos.index') }}"
                        class="btn btn-light"
                    >
                        <i class="icon ion-md-return-left text-primary"></i>
                        @lang('crud.common.back')
                    </a>

                    <button type="submit" class="btn btn-primary float-right">
                        <i class="icon ion-md-save"></i>
                        @lang('crud.common.create')
                    </button>
                </div>
            </x-form>
        </div>
    </div>
</div>
@endsection

@section('js-script')
    <script>
        $('select[name=toko_id]').on('change', function(){
            let value = $(this).val()

            console.log(value);

            $.get('{{ route('api.services.detail-komoditi') }}', {id: value}, function(response){
                $('#komoditi-container').html(response.view)
            })
        })
    </script>
@endsection
