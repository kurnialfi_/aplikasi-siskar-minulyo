@php $editing = isset($detailKomoditi) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="komoditi_id" label="Komoditi" required>
            @php $selected = old('komoditi_id', ($editing ? $detailKomoditi->komoditi_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Komoditi</option>
            @foreach($komoditis as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="nama"
            label="Nama"
            value="{{ old('nama', ($editing ? $detailKomoditi->nama : '')) }}"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
