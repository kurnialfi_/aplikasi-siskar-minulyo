@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('bloks.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.blok.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.blok.inputs.id')</h5>
                    <span>{{ $blok->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.blok.inputs.nama')</h5>
                    <span>{{ $blok->nama ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('bloks.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Blok::class)
                <a href="{{ route('bloks.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
