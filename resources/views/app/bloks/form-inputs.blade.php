@php $editing = isset($blok) @endphp

<div class="row">
    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="id"
            label="Id"
            value="{{ old('id', ($editing ? $blok->id : '')) }}"
            maxlength="2"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.text
            name="nama"
            label="Nama"
            value="{{ old('nama', ($editing ? $blok->nama : '')) }}"
            maxlength="255"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
