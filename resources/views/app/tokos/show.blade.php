@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('tokos.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.toko.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.toko.inputs.id')</h5>
                    <span>{{ $toko->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.toko.inputs.blok_id')</h5>
                    <span>{{ optional($toko->blok)->id ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.toko.inputs.no_toko')</h5>
                    <span>{{ $toko->no_toko ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.toko.inputs.komoditi_id')</h5>
                    <span
                        >{{ optional($toko->komoditi)->nama ?? '-' }}</span
                    >
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('tokos.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Toko::class)
                <a href="{{ route('tokos.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
