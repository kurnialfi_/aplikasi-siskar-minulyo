@php $editing = isset($toko) @endphp

<div class="row">
    <x-inputs.hidden
        id="id"
        name="id"
        value="{{ old('id', ($editing ? $toko->id : '')) }}"
    ></x-inputs.hidden>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select id="blok_id" name="blok_id" label="Blok" required>
            @php $selected = old('blok_id', ($editing ? $toko->blok_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Blok</option>
            @foreach($bloks as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.text
            id="no_toko"
            onkeyup="isiId()"
            name="no_toko"
            label="No Toko"
            value="{{ old('no_toko', ($editing ? $toko->no_toko : '')) }}"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12">
        <x-inputs.select name="komoditi_id" label="Komoditi" required>
            @php $selected = old('komoditi_id', ($editing ? $toko->komoditi_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Komoditi</option>
            @foreach($komoditis as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

</div>

<script type="text/javascript">
    function isiId() {
      var blok = document.getElementById('blok_id').value;
      var nomor = document.getElementById('no_toko').value;
      document.getElementById('id').value=blok+nomor;
    }
</script>
