<div class="card">
  <div class="card-body">
      <h4 class="card-title">
          Aturan Pencarian Toko
      </h4>

      <div class="mt-4">
          <div class="mb-4">
              <div class="row">
                  <div class="col-md-6">
                      <h5>@lang('crud.detail_toko.inputs.toko_id')</h5>
                      <span>{{ $toko->id }}</span>
                  </div>
                  <div class="col-md-6">
                      <h5>Blok</h5>
                      <span>{{ $blok->nama }}</span>
                  </div>
              </div>
          </div>
          <div class="mb-4">
              <h5>Komoditi</h5>
              <span>{{ $toko->komoditi->nama ?? '-' }}</span>
          </div>
          <div class="mb-4">
              <h5>Detail Komoditi</h5>
              <table class="table table-stripped">
                @if (count($detailKomoditi) != 0)
                    @foreach ($detailKomoditi as $key => $item)
                    <tr>
                        <td>
                          - {{ $item->nama }}
                        </td>
                    </tr>
                    @endforeach
                @else 
                    <tr>
                        <td class="text-center">Belum ada data.</td>
                    </tr>
                @endif
            </table>
          </div>
      </div>
  </div>
</div>