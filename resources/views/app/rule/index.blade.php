@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-5">
        <div class="card">
          <div class="card-body">
              <h4 class="card-title">
                  <a href="{{ route('home') }}" class="mr-4"
                      ><i class="icon ion-md-arrow-back"></i
                  ></a>
                  Silahkan pilih toko
              </h4>
  
              <div class="mt-4">
                  <div class="row">
                    <x-inputs.group class="col-sm-12 col-lg-12">
                      <x-inputs.select name="toko_id" label="Toko" required>
                          <option>Please select the Toko</option>
                          @foreach($tokos as $value => $label)
                          <option value="{{ $label->id }}" >{{ $label->id }}</option>
                          @endforeach
                        </x-inputs.select>
                    </x-inputs.group>
                  </div>
              </div>
          </div>
        </div>
      </div>

      <div class="col-md-7">
        <div id="container">

        </div>
      </div>
</div>
@endsection

@section('js-script')
    <script>
        $('select[name=toko_id]').on('change', function(){
            let value = $(this).val()

            console.log(value);

            $.get('{{ route('rule.toko') }}', {id: value}, function(response){
                $('#container').html(response.view)
            })
        })
    </script>
@endsection