<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko', function (Blueprint $table) {
            $table->string('id', 6)->index();
            $table->string('blok_id', 2)->references('id')->on('blok');
            $table->unsignedBigInteger('komoditi_id');
            $table->string('no_toko', 4);
            $table->float('longitude');
            $table->float('latitude');

            $table->timestamps();

            $table->foreign('blok_id')->references('id')->on('blok');
            $table->foreign('komoditi_id')->references('id')->on('komoditi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko');
    }
}
