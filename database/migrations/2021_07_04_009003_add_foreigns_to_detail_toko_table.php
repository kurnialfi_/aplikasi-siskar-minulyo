<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignsToDetailTokoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_toko', function (Blueprint $table) {
            $table
                ->foreign('toko_id')
                ->references('id')
                ->on('toko');

            $table
                ->foreign('pemilik_id')
                ->references('id')
                ->on('pemilik');

            $table
                ->foreign('detail_komoditi_id')
                ->references('id')
                ->on('detail_komoditi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_toko', function (Blueprint $table) {
            $table->dropForeign(['toko_id']);
            $table->dropForeign(['pemilik_id']);
            $table->dropForeign(['detail_komoditi_id']);
        });
    }
}
