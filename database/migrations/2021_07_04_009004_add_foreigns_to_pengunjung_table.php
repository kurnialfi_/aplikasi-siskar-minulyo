<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignsToPengunjungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengunjung', function (Blueprint $table) {
            $table
                ->foreign('detail_toko_id')
                ->references('id')
                ->on('detail_toko')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengunjung', function (Blueprint $table) {
            $table->dropForeign(['detail_toko_id']);
        });
    }
}
