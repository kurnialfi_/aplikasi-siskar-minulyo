<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTokoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_toko', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('toko_id', 6)->references('id')->on('toko');
            $table->unsignedBigInteger('pemilik_id');
            $table->unsignedBigInteger('detail_komoditi_id');
            $table->binary('profill_toko');
            $table->string('deskripsi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_toko');
    }
}
