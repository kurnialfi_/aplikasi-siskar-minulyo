<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTokoKomoditiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_toko_komoditi', function (Blueprint $table) {
            $table->string('toko_id');
            $table->unsignedBigInteger('detail_toko_id');
            $table->unsignedBigInteger('detail_komoditi_id');
            $table->string('komoditi');
            $table->timestamps();

            $table->foreign('toko_id')->references('id')->on('toko');
            $table->foreign('detail_toko_id')->references('id')->on('detail_toko');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_toko_komoditi');
    }
}
