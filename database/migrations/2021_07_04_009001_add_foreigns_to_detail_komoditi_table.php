<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignsToDetailKomoditiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_komoditi', function (Blueprint $table) {
            $table
                ->foreign('komoditi_id')
                ->references('id')
                ->on('komoditi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_komoditi', function (Blueprint $table) {
            $table->dropForeign(['komoditi_id']);
        });
    }
}
