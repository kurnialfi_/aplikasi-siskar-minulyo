<?php

use App\Models\Komoditi;
use Illuminate\Database\Seeder;

class KomoditiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Komoditi::class, 5)->create();
    }
}
