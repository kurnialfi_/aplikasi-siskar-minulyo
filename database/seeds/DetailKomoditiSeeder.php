<?php

use App\Models\DetailKomoditi;
use Illuminate\Database\Seeder;

class DetailKomoditiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DetailKomoditi::class, 5)->create();
    }
}
