<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Adding an admin user
        $user = factory(App\Models\User::class, 1)->create([
            'email' => 'admin@admin.com',
            'password' => \Hash::make('admin'),
        ]);
        $this->call(PermissionsSeeder::class);

        // $this->call(UserSeeder::class);
        // $this->call(BlokSeeder::class);
        // $this->call(DetailKomoditiSeeder::class);
        // $this->call(KomoditiSeeder::class);
        // $this->call(DetailTokoSeeder::class);
        // $this->call(PemilikSeeder::class);
        // $this->call(PengunjungSeeder::class);
        // $this->call(TokoSeeder::class);
    }
}
