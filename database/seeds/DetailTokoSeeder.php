<?php

use App\Models\DetailToko;
use Illuminate\Database\Seeder;

class DetailTokoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DetailToko::class, 5)->create();
    }
}
