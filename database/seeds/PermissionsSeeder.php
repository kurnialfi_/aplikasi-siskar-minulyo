<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create default permissions
        Permission::create(['name' => 'list bloks']);
        Permission::create(['name' => 'view bloks']);
        Permission::create(['name' => 'create bloks']);
        Permission::create(['name' => 'update bloks']);
        Permission::create(['name' => 'delete bloks']);

        Permission::create(['name' => 'list detailkomoditis']);
        Permission::create(['name' => 'view detailkomoditis']);
        Permission::create(['name' => 'create detailkomoditis']);
        Permission::create(['name' => 'update detailkomoditis']);
        Permission::create(['name' => 'delete detailkomoditis']);

        Permission::create(['name' => 'list komoditis']);
        Permission::create(['name' => 'view komoditis']);
        Permission::create(['name' => 'create komoditis']);
        Permission::create(['name' => 'update komoditis']);
        Permission::create(['name' => 'delete komoditis']);

        Permission::create(['name' => 'list detailtokos']);
        Permission::create(['name' => 'view detailtokos']);
        Permission::create(['name' => 'create detailtokos']);
        Permission::create(['name' => 'update detailtokos']);
        Permission::create(['name' => 'delete detailtokos']);

        Permission::create(['name' => 'list tokos']);
        Permission::create(['name' => 'view tokos']);
        Permission::create(['name' => 'create tokos']);
        Permission::create(['name' => 'update tokos']);
        Permission::create(['name' => 'delete tokos']);

        Permission::create(['name' => 'list pemiliks']);
        Permission::create(['name' => 'view pemiliks']);
        Permission::create(['name' => 'create pemiliks']);
        Permission::create(['name' => 'update pemiliks']);
        Permission::create(['name' => 'delete pemiliks']);

        Permission::create(['name' => 'list pengunjungs']);
        Permission::create(['name' => 'view pengunjungs']);
        Permission::create(['name' => 'create pengunjungs']);
        Permission::create(['name' => 'update pengunjungs']);
        Permission::create(['name' => 'delete pengunjungs']);

        // Create user role and assign existing permissions
        $currentPermissions = Permission::all();
        $userRole = Role::create(['name' => 'user']);
        $userRole->givePermissionTo($currentPermissions);

        // Create admin exclusive permissions
        Permission::create(['name' => 'list roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'update roles']);
        Permission::create(['name' => 'delete roles']);

        Permission::create(['name' => 'list permissions']);
        Permission::create(['name' => 'view permissions']);
        Permission::create(['name' => 'create permissions']);
        Permission::create(['name' => 'update permissions']);
        Permission::create(['name' => 'delete permissions']);

        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'update users']);
        Permission::create(['name' => 'delete users']);

        // Create admin role and assign all permissions
        $allPermissions = Permission::all();
        $adminRole = Role::create(['name' => 'super-admin']);
        $adminRole->givePermissionTo($allPermissions);

        $user = \App\Models\User::whereEmail('admin@admin.com')->first();

        if ($user) {
            $user->assignRole($adminRole);
        }
    }
}
