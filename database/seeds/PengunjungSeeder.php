<?php

use App\Models\Pengunjung;
use Illuminate\Database\Seeder;

class PengunjungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pengunjung::class, 5)->create();
    }
}
