<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\Komoditi;

$factory->define(Komoditi::class, function (Faker $faker) {
    return [
        'nama' => $faker->text(255),
    ];
});
