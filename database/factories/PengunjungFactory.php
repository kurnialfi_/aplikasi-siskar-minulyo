<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\Pengunjung;

$factory->define(Pengunjung::class, function (Faker $faker) {
    return [
        'nama' => $faker->text(255),
        'komentar' => $faker->text(255),
        'rating' => '1',
        'detail_toko_id' => factory(App\Models\DetailToko::class),
    ];
});
