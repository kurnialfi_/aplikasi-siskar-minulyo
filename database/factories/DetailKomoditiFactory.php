<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\DetailKomoditi;

$factory->define(DetailKomoditi::class, function (Faker $faker) {
    return [
        'nama' => $faker->text(255),
        'komoditi_id' => factory(App\Models\Komoditi::class),
    ];
});
