<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\Toko;

$factory->define(Toko::class, function (Faker $faker) {
    return [
        'no_toko' => $faker->text(4),
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude,
        'blok_id' => factory(App\Models\Blok::class),
        'komoditi_id' => factory(App\Models\Komoditi::class),
    ];
});
