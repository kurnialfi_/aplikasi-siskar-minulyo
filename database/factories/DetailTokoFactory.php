<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\DetailToko;

$factory->define(DetailToko::class, function (Faker $faker) {
    return [
        'profill_toko' => $faker->word,
        'deskripsi' => $faker->text(255),
        'toko_id' => factory(App\Models\Toko::class),
        'detail_komoditi_id' => factory(App\Models\DetailKomoditi::class),
        'pemilik_id' => factory(App\Models\Pemilik::class),
    ];
});
