<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;

use App\Models\Pemilik;

$factory->define(Pemilik::class, function (Faker $faker) {
    return [
        'nik' => $faker->text(16),
        'nama' => $faker->text(255),
        'alamat' => $faker->text(255),
        'no_telp' => $faker->text(15),
    ];
});
